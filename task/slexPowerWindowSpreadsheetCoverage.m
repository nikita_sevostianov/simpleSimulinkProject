% slexPowerWindowSpreadsheetCoverage
%  This script collects coverage for the powerwindow controller for
%  multiple sets of inputs.  The model,
%  slexPowerWindowCntlCoverageIncrease, is simulated with variouse input
%  sets from the spreadsheet, inputCntlCoverageIncrease.xlsx in order to
%  improve coverage results obtained from the model,
%  slexPowerWindowCntlCoverage.

% Copyright 2015 The MathWorks, Inc.

% cell array containing sheet names to process
sheetNames = { ...
    'Logged' ...
    'LoggedObstacleOffEndStopOn', ...
    'LoggedObstacleOnEndStopOff', ...
    'LoggedObstacleOnEndStopOn', ...
    'DriverLoggedPassengerNeutral', ...
    'DriverDownPassengerNeutral', ...
    'DriverUpPassengerNeutral', ...
    'DriverAutoDownPassengerNeutral', ...
    'DriverAutoUpPassengerNeutral', ...
    'PassengerAutoDownDriverNeutral', ...
    'PassengerAutoUpDriverNeutral'
    };

modelName = 'slexPowerWindowCntlCoverageIncrease';

% If model not loaded
if ~bdIsLoaded( modelName )
    % open system
    open_system( modelName );
end

blkPathToFromSpreadsheet = [ modelName '/Expanded Input'];

% check if file does not exist
if exist('inputCntlCoverageIncrease.xlsx','file') == 0
    %generate spread sheet and populate data
    slexPowerWindowSpreadsheetGeneration;
    slexPowerWindowSpreadsheetAddInputs;
end

% for each sheet generate code coverage
covdata = cell( 1, length( sheetNames ));
simout  = cell( 1, length( sheetNames ));
for kSheet = 1: length( sheetNames )
    
    % set the sheet on the From Spreadsheet block
    set_param( blkPathToFromSpreadsheet, 'SheetName' , sheetNames{ kSheet } );
    
    try
        % use cvsim to collect code coverage
        [ covdata{ kSheet } , simout{kSheet} ] = cvsim( modelName );
    catch ME
        rethrow(ME);
    end
    
end

covTotal = covdata{1};

% for each coverage data object add to total
for kCov = 2: length( covdata )
    
    covTotal = covTotal + covdata{ kCov };
end

proj = slproject.getCurrentProject;
projRoot = proj.RootFolder;

% generate report results
cvhtml( fullfile(projRoot,'work','ControllerCoverage.html'), covTotal );