function slexPowerWindowSpreadsheetAddInputs
% SLEXPOWERWINDOWSPREADSHEETADDINPUTS  Populates spreadsheet template with
% simulation inputs based on inputs logged from the 
% slexPowerWindowCntlCoverage model.  These inputs are expanded from the
% original logged data in order to increase the model coverage.
%
%   Limitations: 
%    
%   Writes to spreadsheet file on Windows only
%

% Copyright 2015 The MathWorks, Inc.

% load inputs from logged coverage case
fromFile = load('inputCntlCoverage.mat');
ds = fromFile.Logsout;

% assign time and inputs to variables
time                  = ds.get(1).Values.Time;
triggerValues         = ds.get(1).Values.Data;
obstacle_off          = ds.get(4).Values.Data;
endstop_off           = ds.get(3).Values.Data;
driver_neutral_log    = ds.get(2).Values.Data(:,1);
driver_up_log         = ds.get(2).Values.Data(:,2);
driver_down_log       = ds.get(2).Values.Data(:,3);
passenger_neutral_log = ds.get(5).Values.Data(:,1);
passenger_up_log      = ds.get(5).Values.Data(:,2);
passenger_down_log    = ds.get(5).Values.Data(:,3);

% create obstacle_on variable
obstacle_on = obstacle_off;
obstacle_on(1:end) = true;

% create endstop_on variable
endstop_on = endstop_off;
endstop_on(1:end) = true;

% Get location of project and set data directory as folder for writing
% spreadsheet file
proj = slproject.getCurrentProject;
projRoot = proj.RootFolder;

% write to the spreadsheet file
spreadSheetFileLocationAndName = fullfile( projRoot,  ...
    'data', 'inputCntlCoverageIncrease.xlsx');

if ispc
    % xlswrite only works on Windows

    % Write logged coverage case
    %
    % passenger logged, driver logged, obstacle off, endstop off
    %
    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral_log driver_up_log driver_down_log passenger_neutral_log passenger_up_log passenger_down_log], ...
        'Logged', 'A2');

    %
    % passenger logged, driver logged, obstacle off, endstop on
    %
    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_on ...
        driver_neutral_log driver_up_log driver_down_log passenger_neutral_log passenger_up_log passenger_down_log], ...
        'LoggedObstacleOffEndStopOn', 'A2');

    %
    % passenger logged, driver logged, obstacle on, endstop off
    %
    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_on endstop_off ...
        driver_neutral_log driver_up_log driver_down_log passenger_neutral_log passenger_up_log passenger_down_log], ...
        'LoggedObstacleOnEndStopOff', 'A2');

    %
    % passenger logged, driver logged, obstacle on, endstop on
    %
    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_on endstop_on ...
        driver_neutral_log driver_up_log driver_down_log passenger_neutral_log passenger_up_log passenger_down_log], ...
        'LoggedObstacleOnEndStopOn', 'A2');

    %
    % passenger neutral, driver logged, obstacle off, endstop off
    %

    passenger_neutral = ones(length( passenger_neutral_log ), 1);
    passenger_up = zeros(length( passenger_up_log ), 1);
    passenger_down = zeros(length( passenger_down_log ) , 1);

    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral_log driver_up_log driver_down_log passenger_neutral passenger_up passenger_down],...
        'DriverLoggedPassengerNeutral', 'A2');

    %
    % passenger neutral, driver down, obstacle off, endstop off
    %
    driver_down = ones( length( driver_down_log ) , 1 );
    driver_up   = zeros( length( driver_up_log ) , 1 );
    driver_neutral   = zeros( length( driver_neutral_log ) , 1 );

    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral driver_up driver_down passenger_neutral passenger_up passenger_down],...
        'DriverDownPassengerNeutral', 'A2');

    %
    % passenger neutral, driver up, obstacle off, endstop off
    %
    driver_down = zeros( length( driver_down_log ) , 1 );
    driver_up   = ones( length( driver_up_log ) , 1 );
    driver_neutral   = zeros( length( driver_neutral_log ) , 1 );

    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral driver_up driver_down passenger_neutral passenger_up passenger_down], ...
        'DriverUpPassengerNeutral', 'A2');

    %
    % passenger neutral, driver auto down, obstacle off, endstop off
    %
    driver_down = zeros( length( driver_down_log ) , 1 );
    driver_down(1:100) = 1;
    driver_up   = zeros( length( driver_up_log ) , 1 );
    driver_neutral   = ones( length( driver_neutral_log ) , 1 );
    driver_neutral(1:100) = 0;

    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral driver_up driver_down passenger_neutral passenger_up passenger_down], ...
        'DriverAutoDownPassengerNeutral', 'A2');

    %
    % passenger neutral, driver auto up, obstacle off, endstop off
    %
    driver_down = zeros( length( driver_down_log ) , 1 );
    driver_up   = zeros( length( driver_up_log ) , 1 );
    driver_up(1:100) = 1;
    driver_neutral   = ones( length( driver_neutral_log ) , 1 );
    driver_neutral(1:100) = 0;

    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral driver_up driver_down passenger_neutral passenger_up passenger_down], ...
        'DriverAutoUpPassengerNeutral', 'A2');

    %
    % passenger auto down, driver neutral, obstacle off, endstop off
    %
    driver_down = zeros( length( driver_down_log ) , 1 );
    driver_up   = zeros( length( driver_up_log ) , 1 );
    %%driver_neutral   = zeros( length( driver_neutral_log ) , 1 );
    driver_neutral   = ones( length( driver_neutral_log ) , 1 );

    passenger_neutral = zeros( length( passenger_neutral_log ) , 1 );
    passenger_up = zeros( length( passenger_up_log ) , 1 );
    passenger_down = zeros( length( passenger_down_log ) , 1 );
    passenger_down(1:100) = 1;
    passenger_neutral(101:end) = 1;

    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral driver_up driver_down passenger_neutral passenger_up passenger_down ], ...
        'PassengerAutoDownDriverNeutral', 'A2');

    %
    % passenger auto up, driver neutral, obstacle off, endstop off
    %
    passenger_neutral = zeros( length( passenger_neutral ) , 1 );
    passenger_up = zeros( length( passenger_up_log ) , 1 );
    passenger_up(1:100) = 1;
    passenger_down = zeros( length( passenger_down_log ) , 1 );
    passenger_neutral(101:end) = 1;

    xlswrite(spreadSheetFileLocationAndName, [time triggerValues obstacle_off endstop_off ...
        driver_neutral driver_up driver_down passenger_neutral passenger_up passenger_down ], ...
        'PassengerAutoUpDriverNeutral', 'A2');

end
