function triggerNames = getTriggerNames( modelName )
% GETTRIGGERNAMES  Finds root trigger blocks in model
%

%   Copyright 2015 The MathWorks, Inc.

triggerNames = {};

%use this handle to the modeled system for root inport
hModeledSys = get_param( modelName,'handle' );

tempTriggerNames = get_param(find_system(hModeledSys,...
    'SearchDepth',1,'BlockType','TriggerPort'),'Name');

if ~isempty(tempTriggerNames)
    
    triggerNames{1} = tempTriggerNames;
    
end
end