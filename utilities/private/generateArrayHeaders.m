function arrayHdrs = generateArrayHeaders( headerBase, arrayDims )
% GENERATEARRAYHEADERS Returns names for column header based on array size of
% input signal
%

%   Copyright 2015 The MathWorks, Inc.

cumProdOfArrayDims = cumprod( arrayDims );
totalNumSignals    = cumProdOfArrayDims(end);
%arrayHdrs = cell(totalNumSignals);

if length( arrayDims ) == 1
    % For each total number of signals 
    for k = 1: totalNumSignals       
        arrayHdrs{k} = [ headerBase '(:,' num2str(k) ')' ];
    end    
else
    % time is not first
    if length( arrayDims ) == 2
        % 2-D signal
        for k = 1:totalNumSignals
            [ind1, ind2] = ind2sub(arrayDims, k);
            arrayHdrs{k} = [ headerBase '(' num2str(ind1) ',' num2str(ind2) ',:)' ];
        end       
    elseif length( arrayDims ) == 3
        %3-D signal
        for k = 1:totalNumSignals        
            [ind1, ind2, ind3] = ind2sub(arrayDims, k);
            arrayHdrs{k} = [ headerBase '(' num2str(ind1) ',' num2str(ind2) ',' num2str(ind3) ',:)' ];
        end
    else
        error( 'Port Dimensions with number of dimensions greater than 3 is not supported' );
    end
    
end