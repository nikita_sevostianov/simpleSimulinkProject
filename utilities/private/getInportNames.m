function inportNames = getInportNames( modelName )
% GETINPORTNAMES  Finds root inport blocks in model
%

%   Copyright 2015 The MathWorks, Inc.

inportNames= {};

%use this handle to the modeled system for root inport
hModeledSys = get_param(modelName,'handle');
%this is for signal inports
tempInportNames = get_param(find_system(hModeledSys,...
    'SearchDepth',1,'BlockType','Inport'),'Name');

%the inportNames is a character array it means we have a only 1
%port
if ischar(tempInportNames)
    %turn inports into a cellarray
    inportNames{1} = tempInportNames;
else
    inportNames = tempInportNames;
end

end