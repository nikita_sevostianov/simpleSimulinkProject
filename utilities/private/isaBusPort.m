function [IS_BUS, busObjName] = isaBusPort( dtString )
% ISABUSPORT Determines if input port is a bus signal
%

%   Copyright 2015 The MathWorks, Inc.

idxOfBus = strfind( dtString, 'Bus:');

% Default outputs
IS_BUS      = false;
busObjName  = [];

% If is not empty
if ~isempty( idxOfBus )
    
    % It is a bus
    IS_BUS = true;
    
    % Get bus name
    idxOfBus = strfind( dtString, 'Bus:');
    busObjName = deblank( dtString( ( idxOfBus + length('Bus:')):end ) );
end

