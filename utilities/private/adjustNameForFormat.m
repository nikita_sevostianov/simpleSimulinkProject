function outNames = adjustNameForFormat( modelName, inportNames )
% ADJUSTNAMEFORFORMAT  Modifies names for column header based on format of input signal
%

%   Copyright 2015 The MathWorks, Inc.

outNames   = {};
isMultiDim = [];
portDims   = {};

% For each port
for kPort = 1: length( inportNames )
    currentBlockPath = [modelName '/' inportNames{kPort}];
    dtString = get_param( currentBlockPath, 'OutDataTypeStr');
    % Is a bus port
    [~, IS_BUS, ~, busObjName] = isNonBuiltInDataType( dtString );
    if IS_BUS 
        
        % Get bus object
        try
            busObjVal = evalin('base',busObjName);
        catch
            error(['Expected Bus Object : ' busObjName ' in the base workspace and it was not present']);
        end
        
        if ~isempty( busObjVal )
            % call get element names
            busElNames = getBusElementNames( busObjVal, inportNames{kPort} );
            outNames = [outNames busElNames];
        end
    else
        [isMultiDim( length( isMultiDim ) + 1), portDims{ length( portDims ) + 1 }] = getPortDimensions( currentBlockPath );
        
        if isMultiDim(end)
            arrayHdrs = generateArrayHeaders( inportNames{kPort}, portDims{end} );
            outNames = [outNames arrayHdrs];
        else
            outNames{ length( outNames) + 1 } = inportNames{kPort};
        end
                
    end
end
