function busElnames = getBusElementNames( busObjVal, rootNodeName )
% GETBUSELEMENTNAMES Returns names for column header based bus object for input
% signal
%

%   Copyright 2015 The MathWorks, Inc.

busElnames = {};
for kEl = 1: length( busObjVal.Elements )
    
    % Get datatype
    dtString = busObjVal.Elements(kEl).DataType;
    [IS_BUS, busObjName] = isaBusPort( dtString );
    
    if IS_BUS
        % Get bus object  
        try
            busObjEl = evalin('base',busObjName);
        catch
            error(['Expected Bus Object : ' busObjName ' in the base workspace and it was not present']);
        end
        
        tempElNames = ...
            getBusElementNames( busObjEl, [ rootNodeName '.' busObjVal.Elements(kEl).Name ] );
        
        busElnames = [busElnames tempElNames];
    else
        % If is scalar and not 1 or is not scalar
        if ( isscalar( busObjVal.Elements(kEl).Dimensions ) && ...
                busObjVal.Elements(kEl).Dimensions ~= 1) || ~isscalar( busObjVal.Elements(kEl).Dimensions )
            
            arrayHdrs = generateArrayHeaders( ...
                [ rootNodeName '.' busObjVal.Elements(kEl).Name ], ...
                busObjVal.Elements(kEl).Dimensions );
            busElnames = [busElnames arrayHdrs];
        else
            % else scalar signal
            busElnames{length(busElnames) + 1} = ...
                [ rootNodeName '.' busObjVal.Elements(kEl).Name ];
        end
    end
end

