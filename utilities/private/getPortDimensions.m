function [isMultiDim, portDims] = getPortDimensions( blkPath )
% GETPORTDIMS Determines signal dimensions for input port
%

%   Copyright 2015 The MathWorks, Inc.

isMultiDim = false;

portDims = str2num(get_param( blkPath,'PortDimensions')); %#ok<ST2NM>

if isscalar( portDims )    
    if portDims == -1
        isMultiDim = false;
    elseif portDims > 1
        isMultiDim = true;
    end    
else    
    if ~isempty(rand( portDims )) && ~isscalar( rand( portDims )  )
        isMultiDim = true;
    end
end