function enableNames = getEnableNames( modelName )
% GETENABLENAMES  Finds root enable blocks in model
%

%   Copyright 2015 The MathWorks, Inc.

enableNames = {};

%use this handle to the modeled system for root inport
hModeledSys = get_param(modelName,'handle');
%this is for Enables
tempEnableNames = get_param(find_system(hModeledSys,...
    'SearchDepth',1,'BlockType','EnablePort'),'Name');

if ~isempty(tempEnableNames)
    
    enableNames{1} = tempEnableNames;
    
end
end