function [IS_ENUM, IS_BUS, IS_NUMERIC_TYPE, dataTypeName] = isNonBuiltInDataType( dataType )
% ISNONBUILTINDATATYPE Determines if input port is a bus signal,
% enumeration, or numeric type
%

%   Copyright 2015 The MathWorks, Inc.

% Enum: myEnum
% Bus: BusObjectx

%DEFAULT
IS_ENUM         =    false;
IS_BUS          =    false;
IS_NUMERIC_TYPE =    false;
dataTypeName    = dataType;

% Check for enumeration
enumIdx = strfind( dataType, 'Enum:' );

if ~isempty( enumIdx )
    IS_ENUM = true;
    dataTypeName = deblank( dataType( enumIdx + length( 'Enum:' ) + 1:end ) );
    return;
end

busIdx  = strfind( dataType, 'Bus:' );

if ~isempty( busIdx )
    IS_BUS = true;
    dataTypeName = deblank( dataType( ( busIdx + length('Bus:')):end ) );
    return;
end

% check base workspace for possible numeric type or bus object
try 
    dataTypeVal = evalin('base', dataType );
    
    if isa( dataTypeVal, 'Simulink.Bus' )
        IS_BUS = true;
        return;
    end
    
    if isa( dataTypeVal, 'Simulink.NumericType' )
        IS_NUMERIC_TYPE = true;
        dataTypeName = dataTypeVal.tostring;
        return;
    end
    
    if isa ( dataTypeVal, 'Simulink.AliasType')
        [IS_ENUM, IS_BUS, IS_NUMERIC_TYPE, dataTypeName] = ...
            isNonBuiltInDataType( dataTypeVal.BaseType );
        return;
    end
    
catch
    
end
