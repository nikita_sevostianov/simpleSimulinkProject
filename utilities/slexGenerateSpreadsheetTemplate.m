function wasSuccessful = slexGenerateSpreadsheetTemplate( modelName, fileName, sheetName )
%  SLEXGENERATESPREADSHEETTEMPLATE Generate a spreadsheet based on the root
%  inports of a model.
%
%   SLEXGENERATESPREADSHEETTEMPLATE( MODELNAME, FILENAME, SHEETNAME ) takes a
%   string containing a Simulink(R) model name, MODELNAME, a string
%   containing a name of spreadsheet file to generate, FILENAME and an
%   optional cell array of strings containing a sheet names to add to the
%   spreadsheet file, SHEETNAME.  The default value for SHEETNAME is
%   {'Sheet1'}. 
%
%   For best results, define data types and data sizes on root inport blocks.  
%   If data sizes are inherited, this script assumes the signal is a 
%   one-dimensional time series.
%
%   Limitations: 
%    
%   Writes to spreadsheet file on Windows only
%
%   Root inport block:
%   * Cannot have names that contain '.', '(', or ')'. 
%   * Cannot output an array of buses. 
%   * Cannot output a function-call trigger signal.
%   * Must have signals with dimensions of 3 or less.
%
%
%   Examples:
%
%   Create template for root inports of sldemo_mdlref_counter_bus to
%   counterBus.xlsx on Sheet1. 
%      slexGenerateSpreadsheetTemplate( 'sldemo_mdlref_counter_bus', 'counterBus.xlsx' )
%
%   Create templates for root inports of sldemo_mdlref_counter_bus to
%   counterBus.xlsx on Run1 and Run2. 
%      slexGenerateSpreadsheetTemplate( 'sldemo_mdlref_counter_bus', 'counterBus.xlsx', {'Run1','Run2'} )
%

%   Copyright 2015 The MathWorks, Inc.


narginchk(2,3)  


if nargin == 2
    sheetName = {'Sheet1'};
end

% initialize output variable to false
wasSuccessful = false;

% load the model if it is not already
if ~bdIsLoaded( modelName )
    load_system( modelName );
end

% Get the inports, enables, and triggers
rawInportNames  = getInportNames( modelName );
inportNames  = adjustNameForFormat( modelName, rawInportNames );
enableNames  = getEnableNames( modelName );
triggerNames = getTriggerNames( modelName );


numPorts = length( inportNames ) + length( enableNames ) + length( triggerNames );
allPortNames = cell( 1, numPorts );

% for each inport
for k = 1: length( inportNames )
    allPortNames{k} = inportNames{k};
end

indexIntoNames = length( inportNames ) + 1;

% for each enable
for k = 1: length( enableNames )
    allPortNames{indexIntoNames} = enableNames{k};
    indexIntoNames = indexIntoNames + 1;
end

% for each trigger
for k = 1: length( triggerNames )
    allPortNames{indexIntoNames} = triggerNames{k};
    indexIntoNames = indexIntoNames + 1;
end

% If root inports exist
if ~isempty( allPortNames )
    % Create column names
    columnNames = [ 'Time' allPortNames ];
    numCols = length( columnNames );
    for k = 1: numCols
        columnNames{ 2, k } = 0;
        
        if k == 1
            columnNames{ 3, k } = 10; % Stop Time
        else
            columnNames{ 3, k } = 0;
        end
    end
    
    % turn off warning about adding new sheet to file
    oldState = warning('off','MATLAB:xlswrite:AddSheet');
    
    if ispc
        % xlswrite only works on Windows
        for index = 1:length(sheetName)
            % write spreadsheet template file
            wasSuccessful = xlswrite( fileName, columnNames, sheetName{index} );
        end
    end
    
    % restore old warning state
    warning(oldState);
end
end
