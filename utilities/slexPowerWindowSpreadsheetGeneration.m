% slexPowerWindowSpreadsheetGeneration                
%  SLEXPOWERWINDOWSPREADSHEETGENERATION Generate a spreadsheet template
%  from the Simulink Power Window Controller example. 
%
%   Limitations: 
%    
%   Writes to spreadsheet file on Windows only
%

% Copyright 2015 The MathWorks, Inc.
% sdassd
mdlName = 'slexPowerWindowControl';

% Get location of project and set data directory as folder to write to
proj = slproject.getCurrentProject;
projRoot = proj.RootFolder;
spreadSheetFileLocationAndName = fullfile( projRoot, 'data', 'inputCntlCoverageIncrease.xlsx');
    

% Open the model
open_system( mdlName );

sheetNames = { ...
    'Logged' ...
    'LoggedObstacleOffEndStopOn', ...
    'LoggedObstacleOnEndStopOff', ...
    'LoggedObstacleOnEndStopOn', ...
    'DriverLoggedPassengerNeutral', ...
    'DriverDownPassengerNeutral', ...
    'DriverUpPassengerNeutral', ...
    'DriverAutoDownPassengerNeutral', ...
    'DriverAutoUpPassengerNeutral', ...
    'PassengerAutoDownDriverNeutral', ...
    'PassengerAutoUpDriverNeutral'
    };

% Generate the spreadsheet 
slexGenerateSpreadsheetTemplate( mdlName, spreadSheetFileLocationAndName, sheetNames);
